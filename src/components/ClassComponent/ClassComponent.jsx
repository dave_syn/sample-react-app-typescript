import React, { Component } from "react";

class ClassComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { search: [], query: "", loading: false };
  }

  componentDidUpdate(prevProps, PrevState) {
    /* ... */
  }

  renderData(data) {
    /* ... */
  }

  render() {
    const { search, query, loading } = this.state;
    const { lang, style } = this.props;
    return (
      <div className="search-box" style={style}>
        {/* ... */}
      </div>
    );
  }
}

export default ClassComponent;
