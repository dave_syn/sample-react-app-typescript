import React, { Component, CSSProperties } from "react";

interface APIType {
  uid: string;
  name: string;
  descr?: string;
}

interface ListPropsType {
  lang?: string;
  style?: CSSProperties;
}

interface ListStateType {
  query: string;
  search: Array<APIType>;
  loading: boolean;
}

class ClassComponent extends Component<ListPropsType, ListStateType> {
  constructor(props: ListPropsType) {
    super(props);
    this.state = { search: [], query: "", loading: false };
  }

  handler() {
    console.log(window.screen);
  }

  componentDidMount() {
    window.addEventListener("resize", this.handler);
  }

  componentDidUpdate(prevProps: ListPropsType, PrevState: ListStateType) {
    /* ... */
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handler);
  }

  renderData(data: APIType) {
    /* ... */
  }

  render() {
    const { search, query, loading } = this.state;
    const { lang, style } = this.props;
    return (
      <div className="search-box" style={style}>
        {/* ... */}
      </div>
    );
  }
}

export default ClassComponent;
