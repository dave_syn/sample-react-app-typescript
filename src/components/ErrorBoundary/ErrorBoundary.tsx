import React, { ErrorInfo, ReactNode } from "react";

interface StateType {
  hasError: boolean;
}

interface PropsType {
  children: ReactNode;
  fallback?: ReactNode;
}

class ErrorBoundary extends React.Component<PropsType, StateType> {
  constructor(props: PropsType) {
    super(props);
    this.state = { hasError: false };
  }

  static defaultProps = {
    fallback: <h1>Something went wrong.</h1>,
  };

  // _ indicates that the parameter is not to be used
  // name it if you need to use its content
  static getDerivedStateFromError(_: Error): StateType {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.log(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return this.props.fallback;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
