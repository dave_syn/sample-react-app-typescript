import React from "react";

const FunctionalComponent = ({ lang, style }) => {
  const [query, setQuery] = useState();
  const [search, setSearch] = useState();
  const [loading, setLoading] = useState(false);

  const renderData = (data) => {
    /* ... */
  };

  return (
    <div className="search-box" style={style}>
      {/* ... */}
    </div>
  );
};

export default FunctionalComponent;
