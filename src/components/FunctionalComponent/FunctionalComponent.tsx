import React, { CSSProperties, useEffect, useState } from "react";

interface APIType {
  uid: string;
  name: string;
  descr: string | null;
}

interface ListPropsType {
  lang?: string;
  style?: CSSProperties;
}

const FunctionalComponent = ({ lang, style }: ListPropsType) => {
  const [query, setQuery] = useState<string>(); // string|undefined
  const [search, setSearch] = useState<APIType>(); // APIType|undefined
  const [loading, setLoading] = useState(false); // Inferred bool|undef

  useEffect(() => {
    const handler = () => {
      console.log(window.screen);
    };
    window.addEventListener("resize", handler);

    // won't compile
    // return true;

    // return cleanup is optional
    // compiles
    return () => {
      window.removeEventListener("resize", handler);
    };
  });

  const renderData = (data: APIType) => {
    /* ... */
  };

  return (
    <div className="search-box" style={style}>
      {/* ... */}
    </div>
  );
};

export default FunctionalComponent;
