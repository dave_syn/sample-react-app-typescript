import React, { lazy, Suspense } from "react";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import Loading from "./components/Loading/Loading";
// import ClassComponent from "./components/ClassComponent/ClassComponent";

const ClassComponent = lazy(
  () => import("./components/ClassComponent/ClassComponent")
);

function App() {
  return (
    <Suspense fallback={<Loading />}>
      <ErrorBoundary>
        <ClassComponent />
      </ErrorBoundary>
    </Suspense>
  );
}

export default App;
