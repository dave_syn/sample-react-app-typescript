import { useEffect, useState } from "react";

export const useScreenResize = () => {
  const [screen, setScreen] = useState<Screen>();

  useEffect(() => {
    const handler = () => {
      setScreen(window.screen);
    };
    window.addEventListener("resize", handler);

    return () => {
      window.removeEventListener("resize", handler);
    };
  });

  return screen;
};

// const Usage = () => {
//   const screen = useScreenResize();
//   return <></>;
// };
