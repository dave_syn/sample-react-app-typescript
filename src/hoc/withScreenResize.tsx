import React, { Component, ReactComponentElement } from "react";

interface StateType {
  screenResized: Screen;
}

const withScreenResize = <P extends object>(
  ComponentToWrap: React.ComponentType<P>
) =>
  class withScreenResizeHOC extends Component<undefined, StateType> {
    handler() {
      this.setState({ screenResized: window.screen });
    }

    componentDidMount() {
      window.addEventListener("resize", this.handler);
    }

    componentWillUnmount() {
      window.removeEventListener("resize", this.handler);
    }

    render() {
      return <ComponentToWrap {...(this.state as P)} />;
    }
  };

export default withScreenResize;

// const Usage = () => <></>;
// export withScreenResize(usage);
