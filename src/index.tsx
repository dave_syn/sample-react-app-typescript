import React, { StrictMode } from "react";
import { render } from "react-dom";
import App from "./App";

import "./index.scss";

// GOOGLE WEB VITALS
import reportWebVitals from "./reportWebVitals";

// PWA
import * as serviceWorker from "./serviceWorker";

render(
  // StrictMode currently helps with:
  //  * Identifying components with unsafe lifecycles
  //  * Warning about legacy string ref API usage
  //  * Warning about deprecated findDOMNode usage
  //  * Detecting unexpected side effects
  //  * Detecting legacy context API
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById("root")
);

// GOOGLE WEB VITALS
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an  analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// PWA
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
